﻿using ShopBridge.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ShopBridge.Controllers
{
    public class InventoryController : ApiController
    {
        private InventoryDbContext db = new InventoryDbContext();

        [HttpGet]
        public async Task<IHttpActionResult> GetInventory(int id)
        {
            InventoryModel inventory = await db.InventoryModels.Where(x => x.Id == id).SingleOrDefaultAsync();
            if(inventory==null)
            {
                return NotFound();
            }
            return Ok(inventory);
        }

        [HttpGet]
        public async Task <IHttpActionResult> GetAllInventory()
        {
           
                var data = await db.InventoryModels.ToListAsync();

                return Ok(data);
            

            
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddInventory([FromBody]InventoryModel model)
        {
            if (ModelState.IsValid)
            {
                db.InventoryModels.Add(model);

                db.SaveChanges();

                return Ok();
            }
            else
            {
                
                return BadRequest(ModelState);
            }
        }

        
        [HttpPut]
        public async Task<IHttpActionResult> UpdateInventory(int id, [FromBody]InventoryModel model)
        {
            if(ModelState.IsValid)
            {
                InventoryModel inventory = await db.InventoryModels.Where(x => x.Id == id).SingleOrDefaultAsync();

                if (inventory != null)
                {
                    
                    inventory.Name = model.Name;
                    inventory.Description = model.Description;
                    inventory.Price = model.Price;
                    db.SaveChanges();

                    
                    return Ok();
                }


                return NotFound();
            }

            else
            {
                
                return BadRequest(ModelState);
            }
        }

        
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteInventory(int id)
        {
            InventoryModel inventory = await db.InventoryModels.Where(x => x.Id == id).SingleOrDefaultAsync();
            if (inventory == null)
            {
                return NotFound();
            }

            db.InventoryModels.Remove(inventory);
            db.SaveChanges();
            return Ok();
        }
    }
}