﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ShopBridge.Models
{
    public class InventoryDbContext: DbContext
    {
        public InventoryDbContext() : base("InventoryDbContext")
        {
            Database.SetInitializer<InventoryDbContext>(new DropCreateDatabaseIfModelChanges<InventoryDbContext>());
        }

        public DbSet<InventoryModel> InventoryModels { get; set; }
    }
}